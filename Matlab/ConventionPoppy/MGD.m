function robot=MGD(vecteur_nombre,robot,number)
Tc=eye(4);
for i=1:length(vecteur_nombre)
      Tc=Tc*robot.data(vecteur_nombre(i)).T;
      switch number
          case 1 
          robot.buste(:,:,i)=Tc;
          case 2
          robot.tete(:,:,i)=Tc;
          case 3
          robot.brasG(:,:,i)=Tc;
          case 4 
          robot.brasD(:,:,i)=Tc;
          case 5 
          robot.jambeG(:,:,i)=Tc;
          case 6 
          robot.jambeD(:,:,i)=Tc;
      end
  end
end