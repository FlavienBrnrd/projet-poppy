function T=TransfoRTL(vector) 
x=vector(1);
y=vector(2);
z=vector(3);
alpha=vector(6); %yaw
beta=vector(5); %pitch
gamma=vector(4); %roll

T=[cos(alpha)*cos(beta) -sin(alpha)*cos(gamma)+cos(alpha)*sin(beta)*sin(gamma) sin(alpha)*sin(gamma)+cos(alpha)*sin(beta)*cos(gamma) x;...
    sin(alpha)*cos(beta) cos(alpha)*cos(gamma)+sin(alpha)*sin(beta)*sin(gamma) -cos(alpha)*sin(gamma)+sin(alpha)*sin(beta)*cos(gamma) y;...
    -sin(beta) cos(beta)*sin(gamma) cos(beta)*cos(gamma) z;...
    0 0 0 1];

end