clear all
close all
clc

Poppy=GenerateDataPoppy('poppy_urdf.txt');

%Buste
Vec_buste=[1:5];
Vec_tete=[1:7];
Vec_brasG=[1:5,8:11];
Vec_brasD=[1:5,12:15];
Vec_jambeG=[16:20];
Vec_jambeD=[21:25];


Poppy=MGD(Vec_buste,Poppy,1);
Poppy=MGD(Vec_tete,Poppy,2);
Poppy=MGD(Vec_brasG,Poppy,3);
Poppy=MGD(Vec_brasD,Poppy,4);
Poppy=MGD(Vec_jambeG,Poppy,5);
Poppy=MGD(Vec_jambeD,Poppy,6);


%% Plot 

figure(1)
hold on
axis equal

%Dessin du buste
line([0 Poppy.buste(1,4,1)],[0 Poppy.buste(2,4,1)],[0 Poppy.buste(3,4,1)],'Color','black','LineWidth',3); 
for i=1:size(Poppy.buste,3)-1
    line([Poppy.buste(1,4,i) Poppy.buste(1,4,i+1)],[Poppy.buste(2,4,i) Poppy.buste(2,4,i+1)],[Poppy.buste(3,4,i) Poppy.buste(3,4,i+1)],'Color','black','LineWidth',3);
    plot3(Poppy.buste(1,4,i),Poppy.buste(2,4,i),Poppy.buste(3,4,i),'+','markersize',5);
end
    plot3(Poppy.buste(1,4,size(Poppy.buste,3)),Poppy.buste(2,4,size(Poppy.buste,3)),Poppy.buste(3,4,size(Poppy.buste,3)),'+','markersize',5);

%Dessin de la tete
for i=1:size(Poppy.tete,3)-1
    line([Poppy.tete(1,4,i) Poppy.tete(1,4,i+1)],[Poppy.tete(2,4,i) Poppy.tete(2,4,i+1)],[Poppy.tete(3,4,i) Poppy.tete(3,4,i+1)],'Color','green');
    plot3(Poppy.tete(1,4,i),Poppy.tete(2,4,i),Poppy.tete(3,4,i),'+','markersize',5);
end
    plot3(Poppy.tete(1,4,size(Poppy.tete,3)),Poppy.tete(2,4,size(Poppy.tete,3)),Poppy.tete(3,4,size(Poppy.tete,3)),'+','markersize',5);

%Dessin du bras gauche
for i=1:size(Poppy.brasG,3)-1
    line([Poppy.brasG(1,4,i) Poppy.brasG(1,4,i+1)],[Poppy.brasG(2,4,i) Poppy.brasG(2,4,i+1)],[Poppy.brasG(3,4,i) Poppy.brasG(3,4,i+1)],'Color','b');
    plot3(Poppy.brasG(1,4,i),Poppy.brasG(2,4,i),Poppy.brasG(3,4,i),'+','markersize',5);
end
    plot3(Poppy.brasG(1,4,size(Poppy.brasG,3)),Poppy.brasG(2,4,size(Poppy.brasG,3)),Poppy.brasG(3,4,size(Poppy.brasG,3)),'+','markersize',5);

%Dessin du bras droit
for i=1:size(Poppy.brasD,3)-1
    line([Poppy.brasD(1,4,i) Poppy.brasD(1,4,i+1)],[Poppy.brasD(2,4,i) Poppy.brasD(2,4,i+1)],[Poppy.brasD(3,4,i) Poppy.brasD(3,4,i+1)],'Color','b');
    plot3(Poppy.brasD(1,4,i),Poppy.brasD(2,4,i),Poppy.brasD(3,4,i),'+','markersize',5);
end
    plot3(Poppy.brasD(1,4,size(Poppy.brasD,3)),Poppy.brasD(2,4,size(Poppy.brasD,3)),Poppy.brasD(3,4,size(Poppy.brasD,3)),'+','markersize',5);

%Dessin de la jambe gauche
line([0 Poppy.jambeG(1,4,1)],[0 Poppy.jambeG(2,4,1)],[0 Poppy.jambeG(3,4,1)],'Color','r'); 
for i=1:size(Poppy.jambeG,3)-1
    line([Poppy.jambeG(1,4,i) Poppy.jambeG(1,4,i+1)],[Poppy.jambeG(2,4,i) Poppy.jambeG(2,4,i+1)],[Poppy.jambeG(3,4,i) Poppy.jambeG(3,4,i+1)],'Color','r');
    plot3(Poppy.jambeG(1,4,i),Poppy.jambeG(2,4,i),Poppy.jambeG(3,4,i),'+','markersize',5);
end
    plot3(Poppy.jambeG(1,4,size(Poppy.jambeG,3)),Poppy.jambeG(2,4,size(Poppy.jambeG,3)),Poppy.jambeG(3,4,size(Poppy.jambeG,3)),'+','markersize',5);

%Dessin de la jambe droite
line([0 Poppy.jambeD(1,4,1)],[0 Poppy.jambeD(2,4,1)],[0 Poppy.jambeD(3,4,1)],'Color','r'); 
for i=1:size(Poppy.jambeD,3)-1
    line([Poppy.jambeD(1,4,i) Poppy.jambeD(1,4,i+1)],[Poppy.jambeD(2,4,i) Poppy.jambeD(2,4,i+1)],[Poppy.jambeD(3,4,i) Poppy.jambeD(3,4,i+1)],'Color','r');
    plot3(Poppy.jambeD(1,4,i),Poppy.jambeD(2,4,i),Poppy.jambeD(3,4,i),'+','markersize',5);
end
    plot3(Poppy.jambeD(1,4,size(Poppy.jambeD,3)),Poppy.jambeD(2,4,size(Poppy.jambeD,3)),Poppy.jambeD(3,4,size(Poppy.jambeD,3)),'+','markersize',5);

