function Poppy=GenerateDataPoppy(filename)
  fileID = fopen(filename);
  C = textscan(fileID,'%s %s %s %s %f %f %f %s %f %f %f %s %s %s %s');
  fclose(fileID);
  
  
  idx=1;
  for i=1:2:size(C{1,1},1)-1
     Poppy.data(idx).name=strcat('T_',C{1,1}{i,1}(2:end-1),'_to_',C{1,3}{i+1,1}(2:end-1)); %generate name of T
     Poppy.data(idx).vector=[C{1,5}(i) C{1,6}(i) C{1,7}(i) C{1,9}(i) C{1,10}(i) C{1,11}(i)]; %generate vector of parameters
     Poppy.data(idx).T=TransfoRTL(Poppy.data(idx).vector); % generate homogeneous matrix with Poppy (rpy) convention
     idx=idx+1;
  end
end