function [R]=rotation(axe, angle)
if axe=='x'
R=[1 0 0 0; 0 cos(angle) -sin(angle) 0; 0 sin(angle) cos(angle) 0; 0 0 0 1];
else if axe=='y'
     R=[cos(angle) 0 sin(angle) 0; 0 1 0 0; -sin(angle) 0 cos(angle) 0; 0 0 0 1];

    else if axe=='z'
         R=[cos(angle) -sin(angle) 0 0; sin(angle) cos(angle) 0 0; 0 0 1 0; 0 0 0 1];

        else disp('Axe incorrect');
                    
        end
    end
end

end