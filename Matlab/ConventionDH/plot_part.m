function plot_part(nom,Part,repere)
% name=inputname(1)
% figure('Name',nom)
hold on
axis equal
grid on


sizevector=10;
% Dessin de la jambe gauche
line([0 1]/sizevector,[0 0],[0 0],'Color','r','LineWidth',2); 
line([0 0],[0 1]/sizevector,[0 0],'Color','g','LineWidth',2); 
line([0 0],[0 0],[0 1]/sizevector,'Color','b','LineWidth',2); 

line([0 Part.MGD(1).T(1,4)],[0 Part.MGD(1).T(2,4)],[0 Part.MGD(1).T(3,4)],'Color',[200/255 200/255 200/255],'LineWidth',0.001); 
for i=1:size(Part.MGD,2)-1
    line([Part.MGD(i).T(1,4) Part.MGD(i+1).T(1,4)],[Part.MGD(i).T(2,4) Part.MGD(i+1).T(2,4)],[Part.MGD(i).T(3,4) Part.MGD(i+1).T(3,4)],'Color',[200/255 200/255 200/255],'LineWidth',2*i+3);%2*i+3
    plot3(Part.MGD(i).T(1,4),Part.MGD(i).T(2,4),Part.MGD(i).T(3,4),'+','markersize',5);
end
    plot3(Part.MGD(size(Part.MGD,2)).T(1,4),Part.MGD(size(Part.MGD,2)).T(2,4),Part.MGD(size(Part.MGD,2)).T(3,4),'+','markersize',5);

    
    for ii=1:size(Part.MGD,2)
 %Plot des reperes
 if (repere==ii | repere=='all' )
  j=1;
 line([Part.MGD(ii).T(1,4) Part.MGD(ii).T(1,4)+Part.MGD(ii).T(1,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],[Part.MGD(ii).T(2,4) Part.MGD(ii).T(2,4)+Part.MGD(ii).T(2,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],[Part.MGD(ii).T(3,4) Part.MGD(ii).T(3,4)+Part.MGD(ii).T(3,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],'Color','r')
  j=2;
 line([Part.MGD(ii).T(1,4) Part.MGD(ii).T(1,4)+Part.MGD(ii).T(1,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],[Part.MGD(ii).T(2,4) Part.MGD(ii).T(2,4)+Part.MGD(ii).T(2,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],[Part.MGD(ii).T(3,4) Part.MGD(ii).T(3,4)+Part.MGD(ii).T(3,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],'Color','g')
j=3;
 line([Part.MGD(ii).T(1,4) Part.MGD(ii).T(1,4)+Part.MGD(ii).T(1,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],[Part.MGD(ii).T(2,4) Part.MGD(ii).T(2,4)+Part.MGD(ii).T(2,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],[Part.MGD(ii).T(3,4) Part.MGD(ii).T(3,4)+Part.MGD(ii).T(3,j)/(norm([Part.MGD(ii).T(1,j) Part.MGD(ii).T(2,j) Part.MGD(ii).T(3,j)])*sizevector)],'Color','b')
 end
    end
    
xlabel('x')
ylabel('y')
zlabel('z')



end