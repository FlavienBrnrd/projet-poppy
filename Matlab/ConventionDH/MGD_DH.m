function [ Robot ] = MGD_DH( DH,mirror,part )
global T_end_buste;

switch part
    case 'jambe'
    Tc=eye(4);
        if mirror==1 %rotation par rapport z de pi pour adapter le repere 0
        Tc=rotation('z',pi)*eye(4);
        end
    
    case 'buste'
         Tc=rotation('z',pi/2)*rotation('x',pi/2)*eye(4);
    
    case 'bras'
        if mirror==1
        Tc=T_end_buste*MatrixTransfo(0,0,0.004,0)*MatrixTransfo(pi/2,0.05,0.1055,0);
        else
        Tc=T_end_buste*MatrixTransfo(0,0,-0.004,0)*MatrixTransfo(-pi/2,0.05,0.1055,0);
%         Tc=MatrixTransfo(-pi/2,0.05,0.1055,0)*MatrixTransfo(0,0,-0.004,0)*T_end_buste;

        end
        
    case 'tete'
        Tc=T_end_buste*rotation('y',pi/2);
         end


    for i=1:size(DH,1)
        Tc=Tc*MatrixTransfo(DH(i,1),DH(i,2),DH(i,3),DH(i,4));
        Robot(i).T=Tc;
    end

end