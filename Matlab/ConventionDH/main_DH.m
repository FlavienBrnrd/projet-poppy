clear all
close all
clc


%% D�claration des variables de rotation theta
thetaJG=[0 pi/2 -pi/2 0 0];
thetaJD=[0 -pi/2 pi/2 0 0]; %Angles oppos�s
% thetaJG=[0 0 0 0 0];
% thetaJD=[0 0 0 0 0];

thetaBuste=[0 -pi/2 -pi/2 pi/2 0];
% thetaBuste=[0 0 0 0 0];

% thetaBG=[0 0 0 0];
thetaBG=[0 pi/2 pi/2 -pi/2];
thetaBD=[0 0 0 0];

thetaT=[0 0];

%% Declaration des param�tres DH (alpha-d-r-theta)
MatriceDH;

%% MGD

Poppy.JambeGauche.MGD=MGD_DH(Poppy.JambeGauche.DH,0,'jambe'); %MGD_DH(Part,mirror)
Poppy.JambeDroite.MGD=MGD_DH(Poppy.JambeDroite.DH,1,'jambe');
Poppy.Buste.MGD=MGD_DH(Poppy.Buste.DH,0,'buste');
global T_end_buste;
T_end_buste=Poppy.Buste.MGD(5).T;
Poppy.BrasGauche.MGD=MGD_DH(Poppy.BrasGauche.DH,0,'bras');
Poppy.BrasDroit.MGD=MGD_DH(Poppy.BrasDroit.DH,1,'bras');
Poppy.Tete.MGD=MGD_DH(Poppy.Tete.DH,0,'tete');

%% Plot

bool_repere=0;
% figure(1)
plot_part('Jambe gauche',Poppy.JambeGauche,bool_repere)
plot_part('Jambe droite',Poppy.JambeDroite,bool_repere)

plot_part('Buste',Poppy.Buste,5)


plot_part('Bras gauche',Poppy.BrasGauche,5)
% plot_part('Bras droit',Poppy.BrasDroit,bool_repere)
% plot_part('Tete',Poppy.Tete,2)
%% Jacobienne
Poppy.JambeGauche=MatrixJ(Poppy.JambeGauche);
Poppy.JambeDroite=MatrixJ(Poppy.JambeDroite);

%% Symbolique
% 
% 
% syms q1 q2 q3 q4 q5 sym_theta
% sym_theta=[q1 q2 q3 q4 q5];
% 
% Poppy.JambeGauche.DHsym=[0     0.0225417  0      sym_theta(1);...
%                       pi/2  0.0439986  0.005  sym_theta(2);...
%                       -pi/2 0          -0.024 sym_theta(3);...
%                        0    0.182      0      sym_theta(4);...
%                        0    0.18       0      sym_theta(5)];
%                    
% Poppy.JambeGauche.MGDsym=MGD_DH(Poppy.JambeGauche.DHsym,0);
% Poppy.JambeGauche.MGDsym(5).T
