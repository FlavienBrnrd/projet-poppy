function [ Part ] = MatrixJ(Part )

 %Coordonnées du point P
 P=Part.MGD(size(Part.MGD,2)).T(1:3,4);
 
 %Calcul de J:
Jc=[];
 for i=1:size(Part.MGD,2)
     z=Part.MGD(i).T(1:3,3);
     Oi=Part.MGD(i).T(1:3,4);
     Jc=[Jc [cross(z,P-Oi);z]];
 end
Part.J=Jc;
end