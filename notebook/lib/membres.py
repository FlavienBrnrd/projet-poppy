import math

import numpy as np


class Membres:
    PI = math.pi

    def __init__(self, group, dh, robot):
        self.group = group
        self.dh = dh
        self.robot = robot

    def matrix_transfo(self, alpha, d, r, theta):
        """

        :param alpha:
        :param d:
        :param r:
        :param theta:
        :return T: Matrice de transformation homogène
        """
        t = np.array([[math.cos(theta), -math.sin(theta), 0, d],
                      [math.cos(alpha) * math.sin(theta), math.cos(alpha) * math.cos(theta), -math.sin(alpha),
                       -r * math.sin(alpha)],
                      [math.sin(alpha) * math.sin(theta), math.sin(alpha) * math.cos(theta), math.cos(alpha),
                       r * math.cos(alpha)],
                      [0, 0, 0, 1]])
        return t

    def calc_mgd(self, mirror=0):
        """

        :param mirror:
        :return mgd: Structure contennant la valeur de la Matrice de tansformation homogene pour chaque articulations
        """
        mgd = []
        if mirror == 1:
            rx = np.array([[1, 0, 0, 0],
                           [0, math.cos(self.PI), -math.sin(self.PI), 0],
                           [0, math.sin(self.PI), math.cos(self.PI), 0],
                           [0, 0, 0, 1]])
            ry = np.array([[0, math.cos(self.PI), -math.sin(self.PI), 0],
                           [0, 1, 0, 0],
                           [0, math.sin(self.PI), math.cos(self.PI), 0],
                           [0, 0, 0, 1]])
            r = rx.dot(ry)
            tc = r.dot(np.identity(4))
        elif mirror == 0:
            tc = np.identity(4)

        for i in range(len(self.dh)):
            tc = tc.dot(self.matrix_transfo(self.dh[i][0], self.dh[i][1], self.dh[i][2], self.dh[i][3]))
            mgd.append(tc)
        return mgd

    def jacobienne(self, mgd):
        """

        :param mgd:
        :return mat_jacob: matrice jacobienne pour le membre associé
        """
        jacob = np.array([])
        z, p_out, p_curr = np.array([]), np.array([]), np.array([])
        last_idx = len(self.group) - 1
        last = mgd[last_idx]
        for i in range(3):
            p_out = np.append(p_out, last[i][3])

        for i in range(len(self.group)):
            for j in range(3):
                z = np.append(z, mgd[i][j][2])
                p_curr = np.append(p_curr, mgd[i][j][3])

            jc = np.cross(z, (p_out - p_curr))
            jc = np.append(jc, z)
            jacob = np.append(jacob, jc)

            p_curr, z = np.array([]), np.array([])
        jacob = jacob.reshape(len(self.group), 6)
        mat_jacob = jacob.transpose()
        return mat_jacob

    def get_min_max(self, robot_conf):
        """

        :return angle_limit: Valeur angulaire min et max  pour chaque moteur du group
        """
        angle_limite = []
        for key, value in robot_conf.items():
            if key == 'motors':
                for i in range(len(self.group)):
                    # angle_limite.append(self.group[i]) # debug
                    angle_limite.append(value[self.group[i]]['angle_limit'])
        return angle_limite

    def gen_traj(self, qi, qf, tf):
        coeffs = np.array([])
        for i in range(len(self.group)):
            a0 = qi[i]
            a1 = 0
            a2 = (3 / (math.pow(tf, 2))) * (qf[i] - qi[i])
            a3 = -(2 / (math.pow(tf, 3))) * (qf[i] - qi[i])
            coeffs = np.append(coeffs, np.array([a0, a1, a2, a3]))
        return coeffs.reshape(len(self.group), 4)

    def eval_traj(self, coeffs, vrep_time):
        vrep_time = float(vrep_time)
        qd, dqd = np.array([]), np.array([])
        pos = np.array([[1],
                        [vrep_time],
                        [(math.pow(vrep_time, 2))],
                        [(math.pow(vrep_time, 3))]]),
        speed = np.array([[0],
                          [1],
                          [2 * vrep_time],
                          [3 * math.pow(vrep_time, 2)]])
        qd = coeffs.dot(pos)
        dqd = coeffs.dot(speed)
        return qd, dqd
