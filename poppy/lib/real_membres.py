import numpy as np
import math


class RealMembre:

    def __init__(self, group, robot):
        self.group = group
        self.robot = robot

    def reset_membre(self, name_mot):
        """

        :param name_mot: Liste des moteurs a reset
        :return:
        """
        for e in name_mot:
            for m in self.robot.motors:
                if m.name == e:
                    m.compliant = False
                    m.goal_position = 0
        print("reset ok")


    def gen_traj(self, qi, qf, tf):
        """

        :param qi: valeur articulaire initiale
        :param qf: valeur articulaire finale
        :param tf: temps max d'execution
        :return: matrice des coefficients des polynome
        """
        coeffs = np.array([])
        for i in range(len(self.group)):
            a0 = qi[i]
            a1 = 0
            a2 = (3 / (math.pow(tf, 2))) * (qf[i] - qi[i])
            a3 = -(2 / (math.pow(tf, 3))) * (qf[i] - qi[i])
            coeffs = np.append(coeffs, np.array([a0, a1, a2, a3]))
        return coeffs.reshape(len(self.group), 4)


    def eval_traj(self, coeffs, vrep_time):
        """

        :param self:
        :param coeffs: valeurs des coefficients du polynome
        :param vrep_time: temps de simulation
        :return: position desiree et vitesse desiree
        """
        vrep_time = float(vrep_time)
        qd, dqd = np.array([]), np.array([])
        pos = np.array([[1],
                        [vrep_time],
                        [(math.pow(vrep_time, 2))],
                        [(math.pow(vrep_time, 3))]]),
        speed = np.array([[0],
                          [1],
                          [2 * vrep_time],
                          [3 * math.pow(vrep_time, 2)]])
        qd = coeffs.dot(pos)
        dqd = coeffs.dot(speed)
        return qd, dqd


poppy_left_leg_config = {
    "controllers": {
        "my_dxl_controller": {
            "port": "/dev/ttyACM0",
            "sync_read": False,
            "attached_motors": [
                "l_leg"
            ]
        }
    },
    "motorgroups": {
        "l_leg": [
            "l_hip_x",
            "l_hip_y",
            "l_hip_z",
            "l_knee_y",
            "l_ankle_y"
        ]
    },
    "motors": {
        "l_hip_x": {
            "offset": 0.0,
            "type": "MX-28",
            "id": 11,
            "angle_limit": [
                -30,
                28.5
            ],
            "orientation": "direct"
        },
        "l_hip_y": {
            "offset": 2.0,
            "type": "MX-64",
            "id": 13,
            "angle_limit": [
                -104,
                84
            ],
            "orientation": "direct"
        },
        "l_hip_z": {
            "offset": 0.0,
            "type": "MX-28",
            "id": 12,
            "angle_limit": [
                -25,
                90
            ],
            "orientation": "indirect"
        },
        "l_knee_y": {
            "offset": 0.0,
            "type": "MX-28",
            "id": 14,
            "angle_limit": [
                -3.5,
                134
            ],
            "orientation": "direct"
        },
        "l_ankle_y": {
            "offset": 0.0,
            "type": "MX-28",
            "id": 15,
            "angle_limit": [
                -45,
                45
            ],
            "orientation": "direct"
        }
    }
}
