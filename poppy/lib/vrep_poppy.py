import time as real_time


class Time:
    """Class Time pour avoir le temps depuis Vrep"""
    def __init__(self, robot):
        self.robot = robot

    def time(self):
        t_simu = self.robot.get_simulation_current_time()
        return t_simu

    def sleep(self, t):
        t0 = self.robot.current_simulation_time
        while (self.robot.get_simulation_current_time() - t0) < t - 0.01:
            real_time.sleep(0.001)
