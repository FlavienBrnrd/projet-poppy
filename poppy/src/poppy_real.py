import matplotlib.pyplot as plt
import json
import time
import pypot.robot
import numpy as np

from lib import real_membres

# ouverture de la configuration du robot (ici jambe gauche) grace au JSON
with open('poppy_left_leg_config.json', 'w') as f:
    config = json.dump(real_membres.poppy_left_leg_config, f, indent=2)

# Creation d'une instance (robot) en fonction du JSON
poppy = pypot.robot.from_json('poppy_left_leg_config.json')

# Debut de la synchronisation entre les moteur qui composent le robot et le code
poppy.start_sync()

# declaration du membre reel sur le robot
l_leg = ['l_hip_x', 'l_hip_z', 'l_hip_y', 'l_knee_y', 'l_ankle_y']  # groupe de moteur
jambe_gauche = real_membres.RealMembre(l_leg, poppy)

# reset des moteurs
jambe_gauche.reset_membre(['l_ankle', 'l_knee_y'])


# variable (type tableau pour stocker les valeur successive)
stock_ankle = np.array([])
stock_knee = np.array([])
stock_time = np.array([])
stock_qd_ankle = np.array([])
stock_qd_knee = np.array([])

# Variables pour la generation des trajectoires
qinit = np.array([0, 0, 0, 0, 0])
qfin = np.array([0, 0, 0, 120, 45])
tf = 10
qd, dqd = np.array([]), np.array([])
coeffs = jambe_gauche.gen_traj(qinit, qfin, tf)

# temps 0 pour la commande sur robot
t0 = time.time()

while time.time() - t0 < tf:
    qd, dqd = np.array([]), np.array([])
    qd, dqd = jambe_gauche.eval_traj(coeffs, time.time() - t0)
    poppy.l_knee_y.goal_position = qd[3]
    poppy.l_ankle_y.goal_position = qd[4]

    stock_knee = np.append(stock_knee, poppy.l_knee_y.present_position)
    stock_qd_knee = np.append(stock_qd_knee, qd[3])
    stock_ankle = np.append(stock_ankle, poppy.l_ankle_y.present_position)
    stock_qd_ankle = np.append(stock_qd_ankle, qd[4])

    stock_time = np.append(stock_time, time.time() - t0)


# plot des trajectoires
plt.plot(stock_time, stock_ankle)
plt.plot(stock_time, stock_qd_ankle)
plt.plot(stock_time, stock_knee)
plt.plot(stock_time, stock_qd_knee)
plt.show()
jambe_gauche.reset_membre(['l_ankle_y', 'l_knee_y'])
# Stop la synchronisation et on ferme se socket
poppy.stop_sync()
poppy.close()
