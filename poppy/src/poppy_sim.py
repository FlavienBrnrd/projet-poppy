import json, math
import pypot.vrep.io as vrep_io
import numpy as np
import time
import pprint as pp
# import utils lib
from lib import membres as mb
from lib import vrep_poppy

# Constants
PI = math.pi

# Start sim vrep
# with open('poppy_humanoid.json', 'r') as ff:
#    config = json.load(ff)
poppy = vrep_io.VrepIO('127.0.0.1', 19997, 'poppy_humanoid.ttt')

# Definition d'un group (membres du robot)
l_leg = ['l_hip_x', 'l_hip_z', 'l_hip_y', 'l_knee_y', 'l_ankle_y']

# Variables articulaire pour le groupe
q = np.array([0, 0, 0, 0, 0])

# Tableau des parametre DH correspondant
l_leg_dh = np.array([[0, 0.0225417, 0, q[0]],
                     [PI / 2, 0.0439986, 0.005, q[1]],
                     [-PI / 2, 0, -0.024, q[2]],
                     [0, 0.182, 0, q[3]],
                     [0, 0.18, 0, q[4]]])

# Variables pour la gen_traj
qi = np.array([0, 0, 0, 0, 0])
qf = np.array([0, -PI/2, PI/2, 0, 0])
dqf = np.array([1, 1, 1, 1, 1])
dqi = np.array([1, 1, 1, 1, 1])
tf = 1

j_gauche = mb.Membres(l_leg, l_leg_dh, robot=poppy)

if __name__ == '__main__':
    poppy.start_simulation()
    qd, dqd = np.array([]), np.array([])
    coeffs = j_gauche.gen_traj(qi, qf, tf)
    t0 = vrep_poppy.Time(poppy).time()

    while vrep_poppy.Time(poppy).time() - t0 < tf:
        qd, dqd = j_gauche.eval_traj(coeffs, (vrep_poppy.Time(poppy).time() - t0))
        for i in range(len(l_leg)):
            poppy.set_motor_position(l_leg[i], qd[i])
    for i in range(len(l_leg)):
        pp.pprint(math.degrees(poppy.get_motor_position(l_leg[i])))
    time.sleep(2)
    poppy.close()
