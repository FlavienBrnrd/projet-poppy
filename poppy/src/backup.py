import matplotlib.pyplot as plt
import json
import time
import pypot.robot
import numpy as np
import math

from lib import real_membres

with open('poppy_left_leg_config.json', 'w') as f:
    config = json.dump(real_membres.poppy_left_leg_config, f, indent=2)

poppy = pypot.robot.from_json('poppy_left_leg_config.json')
poppy.start_sync()
for m in poppy.motors:
    if m.name == 'l_ankle_y':
        m.compliant = False
        m.goal_position = 0
    if m.name == 'l_knee_y':
        m.compliant = False
        m.goal_position = 0

# Wait for the robot to actually reach the base position.
time.sleep(5)

# Do the sinusoidal motions for 10 seconds
t0 = time.time()
stock_ankle = np.array([])
stock_knee = np.array([])
stock_time = np.array([])
stock_qd_ankle = np.array([])
stock_qd_knee = np.array([])
group = ['l_hip_x', 'l_hip_z', 'l_hip_y', 'l_knee_y', 'l_ankle_y']


def gen_traj(qi, qf, tf):
    coeffs = np.array([])
    for i in range(len(group)):
        a0 = qi[i]
        a1 = 0
        a2 = (3 / (math.pow(tf, 2))) * (qf[i] - qi[i])
        a3 = -(2 / (math.pow(tf, 3))) * (qf[i] - qi[i])
        coeffs = np.append(coeffs, np.array([a0, a1, a2, a3]))
    return coeffs.reshape(len(group), 4)


def eval_traj(coeffs, vrep_time):
    vrep_time = float(vrep_time)
    qd, dqd = np.array([]), np.array([])
    pos = np.array([[1],
                    [vrep_time],
                    [(math.pow(vrep_time, 2))],
                    [(math.pow(vrep_time, 3))]]),
    speed = np.array([[0],
                      [1],
                      [2 * vrep_time],
                      [3 * math.pow(vrep_time, 2)]])
    qd = coeffs.dot(pos)
    dqd = coeffs.dot(speed)
    return qd, dqd


# Variables pour la gen_traj
qinit = np.array([0, 0, 0, 0, -40])
qfin = np.array([0, 0, 0, 120, 40])
tf = 1

qd = np.array([])
coeffs = gen_traj(qinit, qfin, tf)

t0 = time.time()
while time.time() - t0 < tf:
    qd, dqd = np.array([]), np.array([])
    qd, dqd = eval_traj(coeffs, time.time() - t0)

    poppy.l_knee_y.goal_position = qd[3]
    poppy.l_ankle_y.goal_position = qd[4]

    stock_knee = np.append(stock_knee, poppy.l_knee_y.present_position)
    stock_qd_knee = np.append(stock_qd_knee, qd[3])
    stock_ankle = np.append(stock_ankle, poppy.l_ankle_y.present_position)
    stock_qd_ankle = np.append(stock_qd_ankle, qd[4])

    stock_time = np.append(stock_time, time.time() - t0)

for m in poppy.motors:
    if m.name == 'l_ankle_y':
        m.compliant = False
        m.goal_position = 0
    if m.name == 'l_knee_y':
        m.compliant = False
        m.goal_position = 0


plt.plot(stock_time, stock_ankle)
plt.plot(stock_time, stock_qd_ankle)
plt.plot(stock_time, stock_knee)
plt.plot(stock_time, stock_qd_knee)


plt.show()

poppy.stop_sync()
poppy.close()
